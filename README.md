# README #

This is a sample web app showing how to implement basic auth for your Aerobatic hosted web site. More information can be found in the [Aerobatic documentation](http://www.aerobatic.com/docs/).

The web app can be viewed at [http://auth-demo.aerobatic.io/](http://auth-demo.aerobatic.io/)
